#include <sstream>
#include <string>
#include "yaml-cpp/yaml.h"
#include "gtest/gtest.h"
using namespace std;
using namespace std::literals;

TEST(yaml, simple) {
	ostringstream buf;
	YAML::Emitter yaml{buf};
	
	yaml << YAML::BeginSeq 
         << "Anna" << "Berit" << "Carin"
         << YAML::EndSeq;	
	string expected = 
R"(
- Anna
- Berit
- Carin
)";	
	ASSERT_EQ("\n"s + buf.str() + "\n"s, expected);
}
