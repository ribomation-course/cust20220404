
# 1) Default

    cmake -S . -B bld/default
    cmake --build bld/default
    cmake --build bld/default --target run

# 2) Build Type: Release

    cmake -DCMAKE_BUILD_TYPE=Release -S . -B bld/release
    cmake --build bld/release -- VERBOSE=1
    cmake --build bld/release --target run

# 3) Compiler: CLang C/C++

    cmake -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_C_COMPILER=clang -S . -B bld/clang
    cmake --build bld/clang -- VERBOSE=1
    cmake --build bld/clang --target run

# 4) Build Tool: Ninja

    cmake -G Ninja -S . -B bld/ninja
    cmake --build bld/ninja -- -v
    cmake --build bld/ninja --target run

# Fortran

    cmake -G Ninja -S fortran-project -B bld/fortran
    cmake --build bld/fortran -- -v
    cmake --build bld/fortran --target run

# Clean

    rm -rf bld


