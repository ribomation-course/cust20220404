cmake_minimum_required(VERSION 3.12)
project(loops LANGUAGES NONE)

message("--- numbers ---")
foreach(k RANGE 1 10)
    message("${k}")
endforeach()

message("--- words ---")
foreach(k IN ITEMS hi hello howdy)
    message("${k}")
endforeach()
