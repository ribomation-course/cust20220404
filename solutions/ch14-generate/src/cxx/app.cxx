#include <iostream>
#include <iomanip>
#include <string>
#include "build-info.hxx"
using namespace std;
using namespace std::literals;

int main() {
	cout << "User name  : " << BuildInfo::userName() << endl;
	cout << "App name   : " << BuildInfo::appName() << endl;
	cout << "App version: " << BuildInfo::appVersion() << endl;
	cout << "Build date : " << BuildInfo::buildDate() << endl;
	cout << "Commit ID  : " << BuildInfo::commitId() << endl;
	
	return 0;
}

