
# Build
## Shapes
    cmake -S shapes -B bld/shapes
    cmake --build bld/shapes
## SHM
    cmake -S shared-mem -B bld/shm
    cmake --build bld/shm

# Execute

    ./bld/shapes/shapes
    ./bld/shm/shm-app

# Clean
    rm -rf bld

