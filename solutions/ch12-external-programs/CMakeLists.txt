cmake_minimum_required(VERSION 3.12)
project(hello LANGUAGES	CXX)

set(CMAKE_CXX_STANDARD			17)
set(CMAKE_CXX_STANDARD_REQUIRED	ON)
set(CMAKE_CXX_EXTENSIONS 		OFF)

add_executable(hello src/hello.cxx)

add_custom_command(OUTPUT hello.md5
	COMMAND ${CMAKE_COMMAND} -E 
			md5sum ${CMAKE_BINARY_DIR}/hello | cut -d " " -f 1 > hello.md5
)
add_custom_command(OUTPUT hello.sha1
	COMMAND ${CMAKE_COMMAND} -E 
			sha1sum ${CMAKE_BINARY_DIR}/hello | cut -d " " -f 1 > hello.sha1
)
add_custom_command(OUTPUT hello.tar.gz
	COMMAND ${CMAKE_COMMAND} -E 
		tar cvfz hello.tar.gz hello hello.md5 hello.sha1
)

add_custom_target(checksum
	DEPENDS hello hello.md5 hello.sha1 hello.tar.gz
)

add_custom_target(run
	COMMAND $<TARGET_FILE:hello>
	
	COMMAND ${CMAKE_COMMAND} -E echo "--- MD5 ---"
	COMMAND cat ${CMAKE_BINARY_DIR}/hello.md5

	COMMAND ${CMAKE_COMMAND} -E echo "--- SHA1 ---"
	COMMAND cat ${CMAKE_BINARY_DIR}/hello.sha1

	COMMAND ${CMAKE_COMMAND} -E echo "--- TAR ---"
	COMMAND ${CMAKE_COMMAND} -E tar tvfz ${CMAKE_BINARY_DIR}/hello.tar.gz
)
