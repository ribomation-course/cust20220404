#define CATCH_CONFIG_MAIN   //once per executable
#include "catch.hpp"

using BigInt = unsigned long long;

BigInt sum(unsigned n) {
    return n * (n + 1) / 2;
}

SCENARIO("testing sum()", "[base]") {
    GIVEN("a lousy function") {
        WHEN("the arg is 10") {
            THEN("it should return 55") {
                REQUIRE(sum(10) == 55);
            }
        }
    }
}

