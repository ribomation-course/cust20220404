#include <iostream>
#include <random>
using namespace std;

int main() {
    uniform_int_distribution<>  d{1,100};
    random_device r;

    auto result = d(r) % 2;
    cout << "result was: " << result << endl;
    return result;
}

