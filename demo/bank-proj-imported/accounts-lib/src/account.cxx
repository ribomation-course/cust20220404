
#include <string>
#include "account.hxx"
#include "util.hxx"
using namespace std;

Account::Account(int balance) 
	: balance{balance} {
	string accno = toUpperCase("hsbc-123-4567");
}

int Account::getBalance() {
	return balance;
}

int Account::update(int amount) {
	balance += amount;
	return balance;
}

