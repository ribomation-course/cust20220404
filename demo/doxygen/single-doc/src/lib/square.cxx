#include "square.hxx"
#include <iostream>

using namespace std;

namespace ribomation::shapes {
    Square::~Square() {
        cout << "~Square() " << this << endl;
    }
}
