cmake_minimum_required(VERSION 3.18)
project(doxygen_simple)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)

set(LIB shape circle rect square triangle)
foreach(name ${LIB})
    list(APPEND LIB_SRC "src/lib/${name}.hxx")
    list(APPEND LIB_SRC "src/lib/${name}.cxx")
endforeach()

set(GEN shape-generator)
foreach(name ${GEN})
    list(APPEND GEN_SRC "src/app/${name}.hxx")    
    list(APPEND GEN_SRC "src/app/${name}.cxx")    
endforeach()

set(APP_SRC "src/app/shapes-app.cxx")

foreach(file ${LIB_SRC})
    message(STATUS ${file})
endforeach()
foreach(file ${GEN_SRC})
    message(STATUS ${file})
endforeach()

add_executable(shapes)
target_sources(shapes PRIVATE ${LIB_SRC} ${GEN_SRC} ${APP_SRC})
target_compile_options(shapes PRIVATE ${WARN})
target_include_directories(shapes PRIVATE src/lib src/app)

find_package(Doxygen)
if (DOXYGEN_FOUND)
    set(DOXYGEN_OUTPUT_DIRECTORY "docs")
    
    set(DOXYGEN_PROJECT_NAME "Shapes App")
    set(DOXYGEN_PROJECT_NUMBER "Version 42")
    set(DOXYGEN_PROJECT_BRIEF "Just a simple C++ classic OOP app")
    set(DOXYGEN_PROJECT_LOGO "src/docs/images/logo.png")
    set(DOXYGEN_HTML_EXTRA_STYLESHEET "src/docs/style.css")
    set(DOXYGEN_IMAGE_PATH "src/docs/images")
    
    set(DOXYGEN_HTML_TIMESTAMP TRUE)
    set(DOXYGEN_HTML_DYNAMIC_SECTIONS TRUE)
    set(DOXYGEN_SHOW_USED_FILES FALSE)
    set(DOXYGEN_SHOW_FILES FALSE)
    set(DOXYGEN_EXTRACT_ALL TRUE)
    set(DOXYGEN_JAVADOC_AUTOBRIEF TRUE)
    set(DOXYGEN_GENERATE_TREEVIEW TRUE)
    set(DOXYGEN_DISABLE_INDEX TRUE)
    set(DOXYGEN_BUILTIN_STL_SUPPORT TRUE)
    set(DOXYGEN_TOC_INCLUDE_HEADINGS 3)
    
    doxygen_add_docs(docs "src/lib" "src/app" "src/docs/pages")
endif (DOXYGEN_FOUND)

