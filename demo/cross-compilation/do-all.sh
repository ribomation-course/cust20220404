#!/usr/bin/env bash
set -eux

rm -rf build
cmake -G Ninja -S . -B build 
cmake --build build --target bld
cmake --build build --target test
ls -l build/linux/hello*
ls -l build/mingw/hello*


