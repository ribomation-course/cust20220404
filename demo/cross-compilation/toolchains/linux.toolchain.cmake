set(CMAKE_SYSTEM_NAME                   Linux)

set(CMAKE_C_COMPILER                    gcc)
set(CMAKE_C_COMPILER_WORKS              true)
set(CMAKE_CXX_COMPILER                  g++)
set(CMAKE_CXX_COMPILER_WORKS            true)

#set(CMAKE_FIND_ROOT_PATH                /usr/x86_64-w64-mingw32)
#set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE   ONLY)
#set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY   ONLY)
#set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM   NEVER)

